//
//  MBNoteTableViewCell.m
//  NotesMbrace
//
//  Created by Albert Donaire on 10/05/14.
//  Copyright (c) 2014 mbrace. All rights reserved.
//

#import "MBNoteTableViewCell.h"

@implementation MBNoteTableViewCell
static NSString *const _reuseIdentifier = @"MBNoteTableViewCell";

+ (NSString*)reuseIdentifier
{
    return _reuseIdentifier;
}

-(void)configureWithNote:(Note*)note
{
    // Fix iOS7 UITextView bug as discussed here:
    // https://stackoverflow.com/questions/19121367/uitextviews-in-a-uitableview-link-detection-bug-in-ios-7
    self.textView.text = nil;
    
    if (note.text != nil && ![note.text isEqualToString:@""]) {
        self.textView.text = note.text;
        self.textView.textColor = [UIColor blackColor];
    } else {
        self.textView.text = [Note emptyNoteText];
        self.textView.textColor = [UIColor grayColor];
    }
}

@end
