//
//  MBConstants.h
//  NotesMbrace
//
//  Created by Albert Donaire on 10/05/14.
//  Copyright (c) 2014 mbrace. All rights reserved.
//

static NSString *const MBUserDefaults_DataIsImported = @"MBUserDefaults_DataIsImported";

static CGFloat MBNoteCellHeightMargin = 10.0f;