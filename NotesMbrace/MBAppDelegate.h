//
//  MBAppDelegate.h
//  NotesMbrace
//
//  Created by Albert Donaire on 10/05/14.
//  Copyright (c) 2014 mbrace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
