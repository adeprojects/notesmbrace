//
//  main.m
//  NotesMbrace
//
//  Created by Albert Donaire on 10/05/14.
//  Copyright (c) 2014 mbrace. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MBAppDelegate class]));
    }
}
