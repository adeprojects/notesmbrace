//
//  MBNoteTableViewCell.h
//  NotesMbrace
//
//  Created by Albert Donaire on 10/05/14.
//  Copyright (c) 2014 mbrace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Note.h"

@interface MBNoteTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UITextView *textView;

+ (NSString*)reuseIdentifier;
-(void)configureWithNote:(Note*)note;
@end
