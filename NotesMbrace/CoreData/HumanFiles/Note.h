#import "_Note.h"

@interface Note : _Note {}
+(NSString*)emptyNoteText;
+(BOOL)importInBackground;
@end
