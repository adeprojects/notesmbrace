// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Note.m instead.

#import "_Note.h"

const struct NoteAttributes NoteAttributes = {
	.id = @"id",
	.noteID = @"noteID",
	.text = @"text",
};

const struct NoteRelationships NoteRelationships = {
};

const struct NoteFetchedProperties NoteFetchedProperties = {
};

@implementation NoteID
@end

@implementation _Note

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Note" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Note";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Note" inManagedObjectContext:moc_];
}

- (NoteID*)objectID {
	return (NoteID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"noteIDValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"noteID"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic id;



- (int32_t)idValue {
	NSNumber *result = [self id];
	return [result intValue];
}

- (void)setIdValue:(int32_t)value_ {
	[self setId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveIdValue {
	NSNumber *result = [self primitiveId];
	return [result intValue];
}

- (void)setPrimitiveIdValue:(int32_t)value_ {
	[self setPrimitiveId:[NSNumber numberWithInt:value_]];
}





@dynamic noteID;



- (int32_t)noteIDValue {
	NSNumber *result = [self noteID];
	return [result intValue];
}

- (void)setNoteIDValue:(int32_t)value_ {
	[self setNoteID:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveNoteIDValue {
	NSNumber *result = [self primitiveNoteID];
	return [result intValue];
}

- (void)setPrimitiveNoteIDValue:(int32_t)value_ {
	[self setPrimitiveNoteID:[NSNumber numberWithInt:value_]];
}





@dynamic text;











@end
