// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Note.h instead.

#import <CoreData/CoreData.h>


extern const struct NoteAttributes {
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *noteID;
	__unsafe_unretained NSString *text;
} NoteAttributes;

extern const struct NoteRelationships {
} NoteRelationships;

extern const struct NoteFetchedProperties {
} NoteFetchedProperties;






@interface NoteID : NSManagedObjectID {}
@end

@interface _Note : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (NoteID*)objectID;





@property (nonatomic, strong) NSNumber* id;



@property int32_t idValue;
- (int32_t)idValue;
- (void)setIdValue:(int32_t)value_;

//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* noteID;



@property int32_t noteIDValue;
- (int32_t)noteIDValue;
- (void)setNoteIDValue:(int32_t)value_;

//- (BOOL)validateNoteID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* text;



//- (BOOL)validateText:(id*)value_ error:(NSError**)error_;






@end

@interface _Note (CoreDataGeneratedAccessors)

@end

@interface _Note (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveId;
- (void)setPrimitiveId:(NSNumber*)value;

- (int32_t)primitiveIdValue;
- (void)setPrimitiveIdValue:(int32_t)value_;




- (NSNumber*)primitiveNoteID;
- (void)setPrimitiveNoteID:(NSNumber*)value;

- (int32_t)primitiveNoteIDValue;
- (void)setPrimitiveNoteIDValue:(int32_t)value_;




- (NSString*)primitiveText;
- (void)setPrimitiveText:(NSString*)value;




@end
