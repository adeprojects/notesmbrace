# NotesMbrace

There are a couple notes related to the project:

### Mogenerator

I am using mogenerator to generate different files for NSManagedObject in order to write code in them while preventing XCode to destroy changes when the data model changes.

These files are generated on build, so there's a build phase that does this. If mogenerator is not installed in the building computer there are two approaches:

- Delete the build phase, which is pretty easy to do in XCode
or
- Install mogenerator as explained here: https://rentzsch.github.io/mogenerator/ with Homebrew

### Cocoapods

I guess everybody uses nowadays but just in case, in order to build and run, do not open the .codeproj file. Instead open .xcworkspace because CocoaPods needs this to work